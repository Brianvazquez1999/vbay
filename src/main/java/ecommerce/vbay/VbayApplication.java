package ecommerce.vbay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VbayApplication {

	public static void main(String[] args) {
		SpringApplication.run(VbayApplication.class, args);
	}

}
